** How to run the tests from command line **

1. Run all tests on QA environment from command line
`mvn clean verify -Denv=qa -Dcucumber.options="--tags @api=all"`

2. Run test from ItelliJ
Right click on any test from the feature file
Feature files can be found src/test/resources/features

3. View html report
target/site/serenity/index.html

4. Prerequisites for running UI tests resources/webdriver folder should contain appropriate driver foreach browser
serenity.conf file contains the path to the URL that is tested, the browser, environment and headless mode
By default the browser is Chrome and headless mode is set to false
4.1 Run UI tests using Firefox
`mvn clean verify -Ddriver=firefox`
4.2 Run UI tests using Firefox and QA environment
`mvn clean verify -Ddriver=firefox -Denvironment=qa`
