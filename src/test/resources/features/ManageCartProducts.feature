@ui=all
Feature: Manage items in the cart shop

  @ui=addProductsToCart
  Scenario: Add 2 products to the cart
    Given the user opens Driedfruits site
    And check that page title is Driedfruits
    And I add the following product to the cart
      | product_number | 1 |
    And I add the following product to the cart
      | product_number | 3 |
    Then check that the following products were added to the cart
      | product_number | 2 |

  @ui=addAndRemoveProductsToCart
  Scenario: Add 2 products to the cart and remove 1 product
    Given the user opens Driedfruits site
    And check that page title is Driedfruits
    And I add the following product to the cart
      | product_number | 1 |
    And I add the following product to the cart
      | product_number | 3 |
    And remove the following product from the cart
      | product_number | 1 |
    Then check that the following products are present to the cart
      | product_number | 1 |

#  Step failed
#  org.openqa.selenium.UnhandledAlertException: unexpected alert open: {Alert text : Internal Server Error
#  Internal Server Error
#  }
#  (Session info: chrome=90.0.4430.85): Internal Server Error
#  Internal Server Error
