@api=all
Feature: Employees

  @api=get
  Scenario: Retrieves all employees and counts the number of employees with age number higher than 30
    Given the user calls GET for EMPLOYEE
    And check that status code is 200
    And check that response time is less than 1500 milliseconds
    Then check that the number of employees with age higher than 30 is 16

  # Adding new employees is not working
  @api=post
  Scenario: Adds new employee with age higher than 30
    Given the user calls POST for EMPLOYEE
      | name   | EmployeeName |
      | salary | 100000       |
      | age    | 40           |
    And check that status code is 200
    And check that response time is less than 1500 milliseconds
    Then check that the following employee was added

  # Updating an employee page is empy http://dummy.restapiexample.com/update. There is no documentation on how to update an employee
  @api=put
  Scenario: Update employee with id 20
    Given the user calls PUT for EMPLOYEE
      | id   | 20 |
    And check that status code is 200
    And check that response time is less than 1500 milliseconds
    Then check that the employee was updated
      | id | 20 |

  # This test is failing because adding new employees is not working
  @api=post
  Scenario: Add new employee with age higher than 30 and check that the number of employees with age greater than 30 increased with 1
    Given the user calls POST for EMPLOYEE
      | name   | EmployeeName |
      | salary | 100000       |
      | age    | 31           |
    And check that status code is 200
    And check that response time is less than 1500 milliseconds
    And the user calls GET for EMPLOYEE
    Then check that the number of employees with age higher than 30 is 17

  @api=delete
  Scenario: Add new employee, delete the new employee and check that the new employee was deleted
    Given the user calls POST for EMPLOYEE
      | name   | EmployeeName |
      | salary | 100000       |
      | age    | 31           |
    And check that status code is 200
    And check that response time is less than 1500 milliseconds
    And the user calls DELETE for EMPLOYEE
    And check that status code is 200
    And check that response time is less than 1500 milliseconds
    Then check that EMPLOYEE was deleted

