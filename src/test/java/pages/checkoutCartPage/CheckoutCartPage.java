package pages.checkoutCartPage;

import locators.checkoutCart.CheckoutCartPageLocators;
import net.thucydides.core.annotations.Step;

public class CheckoutCartPage {

    CheckoutCartPageLocators checkoutCartPageLocators;

    @Step
    public int getNumberOfProductsInCheckoutCart(){
        return checkoutCartPageLocators.getNumberOfProductsInCheckoutCart();
    }

    @Step
    public void removeProductFromCartByIndex(String productIndex){
        checkoutCartPageLocators.removeProductFromCartByIndex(productIndex);
    }
}
