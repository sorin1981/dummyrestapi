package pages.landing;

import locators.landing.LandingPageLocators;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LandingPage {

    LandingPageLocators landingPageLocators;

    @Step
    public void openApplication(){
        landingPageLocators.open();
        landingPageLocators.maximizeWindow();
    }

    @Step
    public void addProductToCartByIndex(String productIndex){
        landingPageLocators.addProductToCartByIndex(productIndex);
    }

    @Step
    public void checkThatPageTitleIsDriedfruits(String pageTitle){
        assertThat(pageTitle, equalTo(getPageTitle()));
    }

    @Step
    public void checkIfPopupIsPresentAndClose(){
        landingPageLocators.checkIfPopupIsPresentAndClose();
    }

    @Step
    public void checkIfAcceptSettingsIsPresentAndClose(){
        landingPageLocators.checkIfAcceptSettingsIsPresentAndClose();
    }

    @Step
    public void checkThatCartButtonContainsProductsInTheName(String productCount){
        landingPageLocators.checkThatCartButtonContainsProductsInTheName(productCount);
    }

    @Step
    public void clickCartButton(){
        landingPageLocators.clickCartButton();
    }

    @Step
    public String getPageURL(){
        return landingPageLocators.getPageURL();
    }

    @Step
    public String getPageTitle(){
        return landingPageLocators.getPageTitle();
    }

}
