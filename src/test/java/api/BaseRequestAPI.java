package api;

import net.serenitybdd.cucumber.CucumberWithSerenity;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseRequestAPI{

    private static final Logger LOGGER = LoggerFactory.getLogger(CucumberWithSerenity.class);

    private String token;

    public void setToken(String token){
        this.token = token;
    }

    public String getToken(){
        return token;
    }

    @Step
    public ResponseOptions<Response> InvokeAPI(String url, String method, Map<String, String> headersParams, Object bodyParams, boolean requireAuthorization){

        RequestSpecBuilder builder = new RequestSpecBuilder();
        headersParams.forEach(builder::addHeader);
        if (bodyParams != null) {
            builder.setBody(bodyParams);
        }
        LOGGER.info("Calling InvokeAPI using url={}, method={}, requiresAuthorization={}, contentType={}, accept={}", url, method, requireAuthorization, headersParams.get("Content-Type"), headersParams.get("Accept"));
        return setTokenAndCallApiMethod(url, method, builder, requireAuthorization);
    }

    public ResponseOptions<Response> setTokenAndCallApiMethod(String url, String method, RequestSpecBuilder builder, boolean requireAuthorization){
        if (requireAuthorization)//authorization
        {
            token = getToken();
            LOGGER.info("TOKEN {}", token);
            builder.addHeader("token", token);
        }

        RequestSpecification requestSpec = builder.build();
        RequestSpecification request = RestAssured.given();
        request.spec(requestSpec);

        LOGGER.info("REQUEST {}", request.log().all());

        if(method.equals("GET"))
            return request.get(url);
        else if(method.equals("POST"))
            return request.post(url);
        else if(method.equals("PUT"))
            return request.put(url);
        else if(method.equals("DELETE"))
            return request.delete(url);
        return null;
    }

    @Step
    public ResponseOptions<Response> Get(String url, Map<String, String> headerParamas, Object bodyParamas, boolean authentication){
        return InvokeAPI(url, "GET", headerParamas, bodyParamas, authentication);
    }

    @Step
    public ResponseOptions<Response> Post(String url, Map<String, String> headerParamas, Object bodyParamas, boolean authentication){
        return InvokeAPI(url, "POST", headerParamas, bodyParamas, authentication);
    }

    @Step
    public ResponseOptions<Response> Put(String url, Map<String, String> headerParams, Object bodyParams, boolean authentication){
        return InvokeAPI(url, "PUT", headerParams, bodyParams, authentication);
    }

    @Step
    public ResponseOptions<Response> Delete(String url, Map<String, String> headerParamas, Map<String, String> bodyParamas, boolean authentication){
        return InvokeAPI(url, "DELETE", headerParamas, bodyParamas, authentication);
    }
}

