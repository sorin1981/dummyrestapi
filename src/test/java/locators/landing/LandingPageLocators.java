package locators.landing;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

public class LandingPageLocators extends PageObject {

    private String BUTTON_NAME = "//input[contains(@ng-reflect-name, 'username')]";
    private String RECOMMENDED_PRODUCT_ITEMS = "//div[contains(@class, 'swiper swiper-has-pages')]/div[contains(@class, 'swiper-container swiper-container-horizontal')]/div[contains(@class, 'swiper-wrapper product-grid')]/div";
    private String PRODUCT = ".//div/div[2]/div[contains(@class, 'buttons-wrapper')]/div/div/a";
    private String POPUP_CLOSE_BUTTON = "//button[contains(@class, 'btn popup-close')]";
    private String CART_BUTTON = "cart-total";
    private String ACCEPT_SETTINGS_BUTTON = "//button[contains(@class, 'align-right primary slidedown-button')]";

    private static final Logger LOGGER = LoggerFactory.getLogger(CucumberWithSerenity.class);

    public String getPageTitle(){
        return getDriver().getTitle();
    }

    public String getPageURL(){
        return getDriver().getCurrentUrl();
    }

    public void maximizeWindow(){
        getDriver().manage().window().setSize(new Dimension(1920, 1080));
    }

    public void addProductToCartByIndex(String productIndex){
        List<WebElement> recommendedProducts = getDriver().findElements(By.xpath(RECOMMENDED_PRODUCT_ITEMS));

        for (int i = 0; i < recommendedProducts.size(); i++){
            if(i == Integer.parseInt(productIndex)){
                recommendedProducts.get(i).findElement(By.xpath(PRODUCT)).click();
                try{
                    Thread.sleep(3000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public void checkIfPopupIsPresentAndClose(){
        try{
            if($(POPUP_CLOSE_BUTTON).isVisible()){
                $(POPUP_CLOSE_BUTTON).click();
            }
        }
        catch (NoSuchElementException ignored){}
    }

    public void checkThatCartButtonContainsProductsInTheName(String productCount){
        assertThat(getDriver().findElement(By.id(CART_BUTTON)).getText(), containsText(productCount + " produs(e)"));
    }

    public void checkIfAcceptSettingsIsPresentAndClose(){
        try{
            if($(ACCEPT_SETTINGS_BUTTON).isVisible()){
                $(ACCEPT_SETTINGS_BUTTON).click();
            }
        }
        catch (NoSuchElementException ignored){}
    }

    public void clickCartButton(){
        LOGGER.info("click CART button");
        getDriver().findElement(By.id(CART_BUTTON)).click();
    }
}
