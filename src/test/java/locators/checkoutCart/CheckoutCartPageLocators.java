package locators.checkoutCart;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CheckoutCartPageLocators extends PageObject {
    private String PRODUCT_TABLE = "//form/div/table/tbody/tr";
    private String REMOVE_BUTTON = ".//button[contains(@class, 'btn btn-remove')]";

    private static final Logger LOGGER = LoggerFactory.getLogger(CucumberWithSerenity.class);

    public int getNumberOfProductsInCheckoutCart(){
        return getDriver().findElements(By.xpath(PRODUCT_TABLE)).size();
    }

    public void removeProductFromCartByIndex(String productIndex){
        List<WebElement> numberOfProductsFromCart = getDriver().findElements(By.xpath(PRODUCT_TABLE));

        for (int i = 0; i < numberOfProductsFromCart.size(); i++){
            if(i == Integer.parseInt(productIndex)){
                numberOfProductsFromCart.get(i).findElement(By.xpath(REMOVE_BUTTON)).click();
                try{
                    Thread.sleep(1000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

}
