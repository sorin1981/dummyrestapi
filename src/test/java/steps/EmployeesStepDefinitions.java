package steps;

import api.BaseRequestAPI;
import common.Configuration;
import dataModels.CreateEmployeeResponse;
import dataModels.EmployeeId;
import dataModels.Employees;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.annotations.Steps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class EmployeesStepDefinitions {

    @Steps
    BaseRequestAPI baseRequestAPI;

    ResponseOptions<Response> response;
    ResponseOptions<Response> getResponse;
    CommonSteps commonSteps = new CommonSteps();

    private static final Logger LOGGER = LoggerFactory.getLogger(CucumberWithSerenity.class);

    private final Configuration configuration = Configuration.getInstance();

    String currentDate = ZonedDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).substring(11, 19).replace(":", "");
    String employees_url = configuration.getBASE_URL() + configuration.getEMPLOYEES();
    String employee_id_url = configuration.getBASE_URL() + configuration.getEMPLOYEE_ID();
    String create_employee_url = configuration.getBASE_URL() + configuration.getCREATE_EMPLOYEE();
    String update_employee_url = configuration.getBASE_URL() + configuration.getUPDATE_EMPLOYEE();
    String delete_employee_url = configuration.getBASE_URL() + configuration.getDELETE_EMPLOYEE();

    private String employee_id = null;

    @Given("the user calls GET for EMPLOYEE")
    public void the_user_calls_get_for_employee() {
        response = baseRequestAPI.Get(employees_url,
                new HashMap<>(),
                new HashMap<>(),
                false);
    }

    @And("check that status code is {int}")
    public void check_that_status_code_is(int httpCode) {
        commonSteps.compareHttpStatusCode(httpCode, response);
    }

    @And("check that response time is less than {long} milliseconds")
    public void check_that_response_time_is_less_than_milliseconds(long expectedTime) {
        commonSteps.compareAPIRunTIme(expectedTime, response);
    }

    @Then("check that the number of employees with age higher than {int} is {int}")
    public void check_that_the_number_of_employees_with_age_higher_than_is(Integer expectedAge, Integer numberOfEmployees) {
        Employees employees = response.getBody().as(Employees.class);
        commonSteps.checkNumberOfEmployeesWithCertainAge(employees, expectedAge, numberOfEmployees);
    }

    @Given("the user calls POST for EMPLOYEE")
    public void theUserCallsPOSTForEMPLOYEE(Map<String, String> employeeDetails) {
        Map<String, String> postBody = new HashMap();

        postBody.put("name", employeeDetails.get("name") + currentDate);
        postBody.put("salary", employeeDetails.get("salary"));
        postBody.put("age", employeeDetails.get("age"));

        response = baseRequestAPI.Post(create_employee_url,
                new HashMap<>(),
                postBody,
                false);

        CreateEmployeeResponse employee = response.getBody().as(CreateEmployeeResponse.class);
        employee_id = employee.getData().getId();
        LOGGER.info("New employee ID: {}", employee_id);
    }

    @Then("check that the following employee was added")
    public void checkThatTheFollowingEmployeeWasAdded() {
        response = baseRequestAPI.Get(employees_url,
                new HashMap<>(),
                new HashMap<>(),
                false);

        Employees employees = response.getBody().as(Employees.class);
        commonSteps.checkThatEmployeeeWasAdded(employees, currentDate);
    }

    @Given("the user calls PUT for EMPLOYEE")
    public void theUserCallsPUTForEMPLOYEE(Map<String, String> employeeId) {
        Map<String, String> putBody = new HashMap();

        putBody.put("name", "update" + currentDate);

        response = baseRequestAPI.Put(update_employee_url + employeeId.get("id"),
                new HashMap<>(),
                putBody,
                false);
        // status code 200
        //{"status":"success","data":[],"message":"Successfully! Record has been updated."}
    }

    @Then("check that the employee was updated")
    public void checkThatTheEmployeeWasUpdated(Map<String, String> employeeId) {
        response = baseRequestAPI.Get(employee_id_url + employeeId.get("id"),
                new HashMap<>(),
                new HashMap<>(),
                false);

        EmployeeId employee = response.getBody().as(EmployeeId.class);

        commonSteps.checkThatEmplooyeWasUpdated(employee, currentDate);
    }

    @And("the user calls DELETE for EMPLOYEE")
    public void theUserCallsDELETEForEMPLOYEE() {
        response = baseRequestAPI.Delete(delete_employee_url + employee_id,
                new HashMap<>(),
                new HashMap<>(),
                false);
    }

    @Then("check that EMPLOYEE was deleted")
    public void checkThatEMPLOYEEWasDeleted() {
        response = baseRequestAPI.Get(employees_url,
                new HashMap<>(),
                new HashMap<>(),
                false);

        Employees employees = response.getBody().as(Employees.class);
        commonSteps.checkThatEmployeeeWasRemoved(employees, currentDate);
    }
}
