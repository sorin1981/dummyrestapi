package steps;

import dataModels.Employee;
import dataModels.EmployeeId;
import dataModels.Employees;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CommonSteps {

    public void compareHttpStatusCode(int httpCode, ResponseOptions<Response> response){
        assertThat(response.statusCode(), equalTo(httpCode));
    }

    public void compareAPIRunTIme(long expectedTime, ResponseOptions<Response> response){
        long actualTimeInMilliseconds = response.timeIn(TimeUnit.MILLISECONDS);
        assertThat(actualTimeInMilliseconds, lessThan(expectedTime));
    }

    public void checkNumberOfEmployeesWithCertainAge(Employees employees, int expectedAge, int expectedNumberOfEmployees){
        int actualNumberOfEmployees = 0;

        List<Employee> result = employees.getData();
        for (Employee employee : result) {
            if (Integer.parseInt(employee.getEmployee_age()) > expectedAge) {
                actualNumberOfEmployees++;
            }
        }
        assertThat(actualNumberOfEmployees, equalTo(expectedNumberOfEmployees));
    }

    public void checkThatEmployeeeWasAdded(Employees employees, String currentDate){
        boolean wasEmployeeAdded = false;

        List<Employee> result = employees.getData();
        for (Employee employee : result) {
            if (employee.getEmployee_name().contains(currentDate)) {
                wasEmployeeAdded = true;
                break;
            }
        }
        assertThat(wasEmployeeAdded, equalTo(true));
    }

    public void checkThatEmployeeeWasRemoved(Employees employees, String currentDate){
        boolean wasEmployeeAdded = false;

        List<Employee> result = employees.getData();
        for (Employee employee : result) {
            if (employee.getEmployee_name().contains(currentDate)) {
                wasEmployeeAdded = true;
                break;
            }
        }
        assertThat(wasEmployeeAdded, equalTo(false));
    }

    public void checkThatEmplooyeWasUpdated(EmployeeId employee, String currentDate){
        boolean wasEmployeeAdded = false;

        if (employee.getData().getEmployee_name().contains(currentDate)) {
            wasEmployeeAdded = true;
        }
        assertThat(wasEmployeeAdded, equalTo(true));
    }

    public int getTheLastEmployeeID(Employees employees){
        return employees.getData().size();
    }
}
