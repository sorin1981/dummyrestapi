package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import pages.checkoutCartPage.CheckoutCartPage;
import pages.landing.LandingPage;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class LandingStepDefinitions {

    @Steps
    LandingPage landingPage;

    @Steps
    CheckoutCartPage checkoutCartPage;

    @Given("the user opens Driedfruits site")
    public void theUserOpensDriedfruitsSite() {
        landingPage.openApplication();
    }

    @And("I add the following product to the cart")
    public void i_add_the_follwoing_product_to_the_cart(Map<String, String> productNumber) {
        landingPage.checkIfPopupIsPresentAndClose();
        landingPage.checkIfAcceptSettingsIsPresentAndClose();
        landingPage.addProductToCartByIndex(productNumber.get("product_number"));
    }

    @Then("check that the following products were added to the cart")
    public void check_that_the_following_products_were_added_to_the_cart(Map<String, String> productCount) {
        landingPage.checkIfAcceptSettingsIsPresentAndClose();
        landingPage.checkThatCartButtonContainsProductsInTheName(productCount.get("product_number"));
        landingPage.clickCartButton();
        assertThat(checkoutCartPage.getNumberOfProductsInCheckoutCart(), equalTo(Integer.parseInt(productCount.get("product_number"))));
    }

    @And("check that page title is {word}")
    public void checkThatPageTitleIsDriedfruits(String pageTitle) {
        landingPage.checkThatPageTitleIsDriedfruits(pageTitle);
    }

    @And("remove the following product from the cart")
    public void removeTheFollowingProductFromTheCart(Map<String, String> productNumber) {
        landingPage.clickCartButton();
        checkoutCartPage.removeProductFromCartByIndex(productNumber.get("product_number"));
    }

    @Then("check that the following products are present to the cart")
    public void checkThatTheFollowingProductsArePresentToTheCart(Map<String, String> productCount) {
        assertThat(checkoutCartPage.getNumberOfProductsInCheckoutCart(), equalTo(Integer.parseInt(productCount.get("product_number"))));
    }
}
