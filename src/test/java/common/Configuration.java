package common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {

    private static Configuration instance;

    private String environment;
    private String BASE_URL;
    private String EMPLOYEES;
    private String EMPLOYEE_ID;
    private String CREATE_EMPLOYEE;
    private String UPDATE_EMPLOYEE;
    private String DELETE_EMPLOYEE;

    private Configuration(){
        Properties allProperties = new Properties();
        Properties environmentProperties = new Properties();

        Properties prop = System.getProperties();
        environment = prop.getProperty("env");

        try{
            if (environment == null){
                environment = "qa";
            }
        }
        catch(NullPointerException e){
            setEnvironment("qa");
        }

        try{
            environmentProperties.load(new FileInputStream("src/test/resources/config_" + environment + ".properties"));
            allProperties.putAll(environmentProperties);

            setBASE_URL(allProperties.getProperty("BASE_URL"));
            setEMPLOYEES(allProperties.getProperty("EMPLOYEES"));
            setEMPLOYEE_ID(allProperties.getProperty("EMPLOYEE_ID"));
            setCREATE_EMPLOYEE(allProperties.getProperty("CREATE_EMPLOYEE"));
            setUPDATE_EMPLOYEE(allProperties.getProperty("UPDATE_EMPLOYEE"));
            setDELETE_EMPLOYEE(allProperties.getProperty("DELETE_EMPLOYEE"));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static Configuration getInstance(){
        if (instance == null){
            instance = new Configuration();
        }
        return instance;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getBASE_URL() {
        return BASE_URL;
    }

    public void setBASE_URL(String BASE_URL) {
        this.BASE_URL = BASE_URL;
    }

    public String getEMPLOYEES() {
        return EMPLOYEES;
    }

    public void setEMPLOYEES(String EMPLOYEES) {
        this.EMPLOYEES = EMPLOYEES;
    }

    public String getEMPLOYEE_ID() {
        return EMPLOYEE_ID;
    }

    public void setEMPLOYEE_ID(String EMPLOYEE_ID) {
        this.EMPLOYEE_ID = EMPLOYEE_ID;
    }

    public String getCREATE_EMPLOYEE() {
        return CREATE_EMPLOYEE;
    }

    public void setCREATE_EMPLOYEE(String CREATE_EMPLOYEE) {
        this.CREATE_EMPLOYEE = CREATE_EMPLOYEE;
    }

    public String getUPDATE_EMPLOYEE() {
        return UPDATE_EMPLOYEE;
    }

    public void setUPDATE_EMPLOYEE(String UPDATE_EMPLOYEE) {
        this.UPDATE_EMPLOYEE = UPDATE_EMPLOYEE;
    }

    public String getDELETE_EMPLOYEE() {
        return DELETE_EMPLOYEE;
    }

    public void setDELETE_EMPLOYEE(String DELETE_EMPLOYEE) {
        this.DELETE_EMPLOYEE = DELETE_EMPLOYEE;
    }
}

